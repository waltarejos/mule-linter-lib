package org.deloitte.dpe

import org.junit.Test

class ExpectationTests {
  NodeChecker nodeChecker

  Expectation simpleSetup() {
    //nodeChecker = new NodeChecker(['core': 'wqer', 'http': 'http://www.mulesoft.org/schema/mule/http'])
    nodeChecker = new NodeChecker(['http': 'http://www.mulesoft.org/schema/mule/http',
                                   'doc':'http://www.mulesoft.org/schema/mule/documentation',
                                   'utils':'http://www.mulesoft.org/schema/mule/utils',
                                    'xsi':'http://www.w3.org/2001/XMLSchema-instance',
                                   'dw':'http://www.mulesoft.org/schema/mule/ee/dw',
                                   'scripting':'http://www.mulesoft.org/schema/mule/scripting'])


//    <mule xmlns:http="http://www.mulesoft.org/schema/mule/http" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
//    xmlns:doc="http://www.mulesoft.org/schema/mule/documentation" xmlns:utils="http://www.mulesoft.org/schema/mule/utils"
//    xsi:schemaLocation="http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd"
//

    Expectation expectation = new Expectation()

    return expectation
  }

  @Test
  void elementNameIsNotFound() {
    def expectation = simpleSetup()

    assert false == expectation.isElementNameFound()
  }

  @Test
  void elementNameIsFound() {
    def expectation = simpleSetup()

    expectation.elementName = 'logger'

    def root = new XmlParser().parseText('<mule> <logger /> </mule>')

    expectation.handleNode("example.xml", root.logger[0], nodeChecker)

    assert expectation.isElementNameFound()
  }

  @Test
  void elementAttributeIsFound() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasAttribute('category')

    def root = new XmlParser().parseText('<mule> <logger category=""/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementAttributeIsFoundWithValue() {
    def expectation = simpleSetup()

    expectation.forElement('logger').withAttributeContainsValue('apiName', 'sb.api-customer-v1.create-customer.start')

    def root = new XmlParser().parseText('<mule> <logger apiName="sb.api-customer-v1.create-customer.start"/></mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementAttributeContainsScript() {
    def expectation = simpleSetup()

    //expectation.forElement('script').withElementContainsText('apiName', '[CDATA[println(\'Hello World\')]')

    expectation.forElement('script').withElementContainsText('apiName', '<![CDATA[println(\'Hello World\')]]>')

  //  def root = new XmlParser().parseText('<script apiName="sb.api-customer-v1.create-customer.start"><![CDATA[println(\'Hello World\')]]></script>')

    def root = new XmlParser().parseText('<script apiName="sb.api-customer-v1.create-customer.start"><![CDATA[println(\'Hello World\')]]></script>')


    expectation.handleNode("example.xml",root, nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }


  @Test
  void elementAttributeIsNotFound() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasAttribute('category')

    def root = new XmlParser().parseText('<mule> <logger/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && !expectation.isPassing()
  }

  @Test(expected = IllegalArgumentException)
  void failureIsRaisedWhenTheSameAttributeIsGivenMoreThanOnce() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasAttribute('category').hasAttribute('category')

    def root = new XmlParser().parseText('<mule> <logger category="odd-category"/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)
  }

  @Test
  void elementAttributeIsNotFoundWhenValueIsDifferent() {
    def expectation = simpleSetup()

    //expectation.forElement('logger').hasAttribute('category').havingValue('correct-category')
    expectation.forElement('logger').hasAttribute('category', 'correct-category')

    def root = new XmlParser().parseText('<mule> <logger category="odd-category"/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && !expectation.isPassing()
  }

  @Test
  void elementAttributeWorksWithListsAndIsFound() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasAttribute('category', ['correct-category'])

    def root = new XmlParser().parseText('<mule> <logger category="correct-category"/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementAttributeWorksWithListsAndIsNotFound() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasAttribute('category', ['correct-category'])

    def root = new XmlParser().parseText('<mule> <logger category="odd-category"/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && !expectation.isPassing()
  }

  @Test(expected = IllegalArgumentException)
  void elementHasParentFailsWhenCalledTwice() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasParent('first').hasParent('second')
  }

  @Test
  void elementHasParentValidates() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasParent('mule')

    def root = new XmlParser().parseText('<mule> <logger category="odd-category"/> </mule>')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementHasPriorSibling() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasPriorSibling('until-successful')

    def root = new XmlParser().parseText('''
<mule>
  <logger />
</mule>
''')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && !expectation.isPassing()
  }

  @Test
  void elementHasFollowingSibling() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasPriorSibling('until-successful')

    def root = new XmlParser().parseText('''
<mule>
  <logger />
</mule>
''')

    expectation.handleNode("example.xml",root.logger[0], nodeChecker)

    assert expectation.isElementFound() && !expectation.isPassing()
  }

  @Test(expected = IllegalArgumentException)
  void elementHasPriorSiblingThrowsExceptionForSameNames() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasPriorSibling('logger')
  }

  @Test(expected = IllegalArgumentException)
  void elementHasFollowingSiblingThrowsExceptionForSameNames() {
    def expectation = simpleSetup()

    expectation.forElement('logger').hasFollowingSibling('logger')
  }

  @Test
  void elementHasPriorSiblingPasses() {
    def expectation = simpleSetup()

    expectation.forElement('http:request').hasPriorSibling('logger')

    def root = new XmlParser().parseText('''
<mule xmlns:http="http://www.mulesoft.org/schema/mule/http"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
  <logger />
  <http:request />
</mule>
''')

    expectation.handleNode("example.xml",root.children()[1], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementHasFollowingSiblingPasses() {
    def expectation = simpleSetup()

    expectation.forElement('http:request').hasFollowingSibling('logger')

    def root = new XmlParser().parseText('''
<mule xmlns:http="http://www.mulesoft.org/schema/mule/http"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="
http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
  <http:request />
  <logger />
</mule>
''')

    expectation.handleNode("example.xml",root.children()[0], nodeChecker)

    assert expectation.isElementFound() && expectation.isPassing()
  }

  @Test
  void elementHasChildPasses() {
    def expectation = simpleSetup()

    expectation.forElement('flow').hasChild('flow-ref')

    def root = new XmlParser().parseText('''
<mule xmlns:http="http://www.mulesoft.org/schema/mule/http"	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:doc="http://www.mulesoft  .org/schema/mule/documentation" xmlns:utils="http://www.mulesoft.org/schema/mule/utils" 
xsi:schemaLocation="http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd">
   <flow name="lint_testFlow" doc:description="Main Flow">
        <logger>hello</logger>
        <http:listener config-ref="HTTP_Listener_Configuration" path="/" doc:name="HTTP"/>
        <utils:pull-http-metadata config-ref="Mule_Utilities__Mule_Utilities_Configuration" doc:name="Mule Utilities"/>
        <flow-ref name="process-sub-flow" doc:name="process-sub-flow"/>
        <flow-ref name="another-sub-flow" doc:name="call-another-sub-flow"/>
        <utils:pull-http-metadata config-ref="Mule_Utilities__Mule_Utilities_Configuration" doc:name="Mule Utilities"/>
        <exception-strategy ref="global-exception-strategy" doc:name="Reference Exception Strategy"/>
    </flow>
</mule>
''')

    expectation.handleNode("example.xml",root.children()[0], nodeChecker)
    println('assert : ' + expectation.isElementFound() + ' ' +  expectation.isPassing())
    assert expectation.isElementFound() && expectation.isPassing()
  }
}
