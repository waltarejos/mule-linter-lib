package org.deloitte.dpe.model

class Infraction {
  String element
  int lineNumber
  String message
  String category
  String value
  String attribute
}
