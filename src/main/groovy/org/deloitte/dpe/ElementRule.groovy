package org.deloitte.dpe

import groovy.util.logging.Slf4j

@Slf4j(category = 'org.deloitte.dpe.msa')
class ElementRule {
  String version
  ExpectationBuilder builder

  ElementRule(ExpectationBuilder builder) {
    this.builder = builder
  }

  ElementRule version(String version) {
    log.info 'Version set to ' + version

    if (this.version != null) {
      def message = 'Can not set version once it has already been set'

      log.error message

      throw new Exception(message)
    }

    return this
  }

  ElementRule element(String name) {
    log.debug 'Picked up element of ' + name

    builder.element(name)

    return this
  }

  ElementRule hasAttribute(String name) {
    log.debug 'Something should have attribute of ' + name

    builder.hasAttribute(name)

    return this
  }

  ElementRule hasParent(String name) {
    log.debug 'hasParent specified ' + name

    builder.hasParent(name)

    return this
  }

  ElementRule hasPriorSibling(String name) {
    log.debug 'hasPriorSibling specified ' + name

    builder.hasPriorSibling(name)

    return this
  }

  ElementRule hasFollowingSibling(String name) {
    log.debug 'hasFollowingSibling specified ' + name

    builder.hasFollowingSibling(name)
    return this
  }

  ElementRule hasChild(String name) {
    log.debug 'hasChild specified ' + name

    builder.hasChild(name)

    return this
  }

  ElementRule withAttribute(String name) {
    log.debug 'withAttribute specified ' + name

    builder.withAttribute(name)

    return this
  }


  ElementRule withAttributeContainsValue(String attribute, String value) {
    log.debug 'attribute ' + attribute + ' containsValue specified ' + value

    builder.withAttributeContainsValue(attribute, value)

    return this
  }

  ElementRule withElementContainsText(String attribute, String value) {
    log.debug 'attribute ' + attribute + ' containsValue specified ' + value

    builder.withElementContainsText(attribute, value)

    return this
  }
}

