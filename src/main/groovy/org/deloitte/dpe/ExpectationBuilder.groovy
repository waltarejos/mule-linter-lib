package org.deloitte.dpe

import groovy.util.logging.Slf4j

@Slf4j(category = 'org.deloitte.dpe.msa')
class ExpectationBuilder {
  Expectation currentExpectation
  List<Expectation> expectations = new ArrayList<Expectation>()

  ExpectationBuilder() {
  }

  void element(String name) {
    Expectation expectation = new Expectation(elementName: name)

    currentExpectation = expectation

    expectations << expectation
  }

  void hasAttribute(String attribute) {
    currentExpectation.hasAttribute(attribute)
  }

  void hasAttribute(String attribute, String value) {
    currentExpectation.hasAttribute(attribute, value)
  }

  void hasAttribute(String attribute, List<String> values) {
    currentExpectation.hasAttribute(attribute, values)
  }

  void hasParent(String parent) {
    currentExpectation.hasParent(parent)
  }

  void hasPriorSibling(String sibling) {
    currentExpectation.hasPriorSibling(sibling)
  }

  void hasFollowingSibling(String sibling) {
    currentExpectation.hasFollowingSibling(sibling)
  }

  void hasChild(String child) {
    currentExpectation.hasChild(child)
  }

  void withAttribute(String attribute) {
    currentExpectation.withAttribute(attribute)
  }

  void withAttributeContainsValue(String attribute, String value) {
    currentExpectation.withAttributeContainsValue(attribute, value)
  }

  void withElementContainsText(String attribute, String value) {
    currentExpectation.withElementContainsText(attribute, value)
  }

  List<Expectation> getExpectations() {
    return expectations
  }
}
