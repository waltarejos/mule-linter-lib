package org.deloitte.dpe

import groovy.util.logging.Slf4j
import org.deloitte.dpe.model.Infraction

/**
 * The "has" operations set things up differently than the "having" operations
 */
@Slf4j(category = 'org.nuisto.msa')
class Expectation {
  boolean elementFound
  boolean passing
  boolean checkForAttribute
  boolean childFound

  String parent

  String priorSibling
  String followingSibling

  String elementName

  String childnode
  String nodeValue
  String attrib
  String mode
  String script
  String filename
  String containsText
  String foundWithValue

  Map<String, List<String> > attributes
  Map<String, List<String> > children
  Map<String, List<String> > matchedNode

  List<Infraction> findings

  int vCtr = 0
  int vIndex = 0
  int vFoundCount
  boolean vResult = true

  Expectation() {
    elementName = null
    checkForAttribute = false
    attributes = [:]
    children = [:]
    nodeValue
    attrib
    matchedNode = [:]
    init()
  }

  void init() {
    findings = []
    elementFound = false
    passing = true
    childFound = false
  }

  /**
   * We have to have some notion of "resetting" based upon a new file.
   *
   * An expectation is based on a file, when we have a new file, then the expectation should be reset.
   *
   * Might turn this into more like event based "onNewFile", but I'm not sure about that.
   */
  void reset() {
    log.debug 'Resetting expectation per new file.'
    init()
  }

  boolean isElementNameFound() {
    return elementFound
  }

  boolean isPassing() {
    return passing
  }

  boolean isChildFound() {
    return childFound
  }

  void handleNode(String filename, Node node, NodeChecker nodeChecker) {

    if (nodeChecker.isMatch(node, elementName)) {
      elementFound = true
      log.debug 'We matched on element {}', elementName
      this.filename = filename

      validateParent(node, nodeChecker)
      println('1')
      validateAttributes(node)
      println('2')
      validatePriorSibling(node, nodeChecker)
      println('3')
      validateFollowingSibling(node, nodeChecker)
      println('4')
      validateChild(node, nodeChecker)
      println('5')
      validateAttributeContainsValue(node,nodeChecker)
println('BEFORE validateAttributeContainsText')
      validateAttributeContainsText(node,nodeChecker)
      println('AFTER validateAttributeContainsText')

    }
  }

  Expectation forElement(String name) {
    elementName = name
    return this
  }

  Expectation hasAttribute(String attribute) {
    return hasAttribute(attribute, (String) null)
  }

  Expectation hasAttribute(String attribute, String value) {
    return hasAttribute(attribute, value == null ? null : [value])
  }

  Expectation hasAttribute(String attribute, List<String> values) {
    checkForAttribute = true
    if (attributes.containsKey(attribute)) throw new IllegalArgumentException('Key already exists')
    attributes.put(attribute, values)

    return this
  }

  Expectation hasParent(String parent) {
    if (this.parent != null) throw new IllegalArgumentException('Parent already specified')

    this.parent = parent

    return this
  }

  Expectation hasPriorSibling(String sibling) {
    if (this.priorSibling != null) throw new IllegalArgumentException('Prior Sibling already specified')
    if (this.elementName == sibling) throw new IllegalArgumentException('Sibling can not be the same as the element')

    this.priorSibling = sibling

    return this
  }

  Expectation hasFollowingSibling(String sibling) {
    if (this.followingSibling != null) throw new IllegalArgumentException('Following Sibling already specified')
    if (this.elementName == sibling) throw new IllegalArgumentException('Sibling can not be the same as the element')

    this.followingSibling = sibling

    return this
  }

  Expectation hasChild(String childnode) {
    if (this.children.get(childnode) != null ) throw new IllegalArgumentException('Child already specified')

    this.children.put(this.elementName, childnode)
    this.childnode = childnode
    childFound = true
    return this
  }

  Expectation withAttribute(String attribute) {
    if (this.nodeValue != null) throw new IllegalArgumentException('Value is already specified')
    this.attrib = attribute
    this.nodeValue = value
    return this
  }

  Expectation withAttributeAndValue(String attribute, String value, String mode) {
    if (this.nodeValue != null) throw new IllegalArgumentException('Value is already specified')
    this.attrib = attribute
    this.nodeValue = value
    this.mode = mode

    return this
  }

  Expectation withAttributeContainsValue(String attribute, String value) {
    if (this.nodeValue != null) throw new IllegalArgumentException('Value is already specified')
    this.attrib = attribute
    this.containsText = value
    this.foundWithValue = "value"
    this.nodeValue = value

    return this
  }

  Expectation withElementContainsText(String attribute, Object value) {
    print('At withElementContainsText\nExpectation > elementContainsText : ')

    if (value == null) throw new IllegalArgumentException('Value is null')
    if (this.nodeValue != null) throw new IllegalArgumentException('Value is already specified')
    this.attrib = attribute
    this.foundWithValue = "text"
    this.script = value
    println('\nExpectation > After Condition : ')
    println('Expectation >  Attribute : ' + attribute +
            " \n| Value : " + value +
            ' \n| this.attrib ' + this.attrib +
            ' \n| Nodevalue ' + this.nodeValue )
    return this
  }

  void validateAttributes(Node node) {
    if (checkForAttribute) {
      passing = false
      def foundEntry = attributes.find { k, v ->

        if (!node.attributes().containsKey(k)) {
          //Node does not contain the attribute we are looking to validate

          findings << new Infraction(
                  element: elementName,
                  message: "Element $elementName does not contain the attribute $k",
                  lineNumber: findLineNumber(node),
                  category: 'InvalidAttribute')
          return false
        }

        String foundValue = node.attribute(k)

        if (v == null) {
          //No value to check against, so this is purely just checking if the attribute exists.
          return true
        }
        else {
          //Check against the list of allowed values
          if (v.contains(foundValue)) {
            return true
          }
          else {
            findings << new Infraction(
                    element: elementName,
                    message: "Element $elementName has attribute $k but $v is an invalid value",
                    lineNumber: findLineNumber(node),
                    category: 'InvalidAttribute')
            return false
          }
        }
      }

      passing = foundEntry != null
    }
  }

  int findLineNumber(Node node) {
    //The _msaLineNumber is set in the "PeakNamespacesXmlParser" class
    String lineNumber = node.@_msaLineNumber
    lineNumber == null ? -1 : lineNumber.toInteger()
  }

  void validateFollowingSibling(Node node, NodeChecker nodeChecker) {
    if (followingSibling == null) return

    def siblings = node.parent().children()
    def thisNodeIndex = siblings.indexOf(node)

    Node foundFollowingSibling = siblings[thisNodeIndex + 1]

    if (foundFollowingSibling == null || !nodeChecker.isMatch(foundFollowingSibling, followingSibling)) {
      findings << new Infraction(
              element: elementName,
              message: "Element $elementName does not have a following sibling of $followingSibling",
              lineNumber: findLineNumber(node),
              category: 'InvalidSibling')

      passing = false
    }
    else {
      passing = true
    }
  }

  void validatePriorSibling(Node node, NodeChecker nodeChecker) {
    if (priorSibling == null) return

    def siblings = node.parent().children()
    def thisNodeIndex = siblings.indexOf(node)

    Node foundPriorSibling = siblings[thisNodeIndex - 1]

    if (foundPriorSibling == null || !nodeChecker.isMatch(foundPriorSibling, priorSibling)) {
      findings << new Infraction(
              element: elementName,
              message: "Element $elementName does not have a prior sibling of $priorSibling",
              lineNumber: findLineNumber(node),
              category: 'InvalidSibling')

      passing = false
    }
    else {
      passing = true
    }
  }

  void validateChild(Node node, NodeChecker nodeChecker) {
    vIndex = 0
    vFoundCount = node.children().size()

    //if (this.nodeValue == null) return

    //if (this.nodeValue.contains(node.children())) return

    println('containsText ' + containsText)
    println('foundWithValue ' + foundWithValue)

    if (this.containsText == null ) return

    if (this.foundWithValue == null) return


    println('containsText ' + containsText)
    println('foundWithValue ' + foundWithValue)
//    println('containsText ' + containsText)
//    println('containsText ' + containsText)

    while (vIndex < node.children().size()){
      vResult = (nodeChecker.isMatch(node.children()[vIndex], this.childnode))

      if (vResult== true)
      {

        break;
        if (vFoundCount > 0)
        {
          println('\nFound : ' + vFoundCount)
        }
        vFoundCount = vFoundCount + 1
        vIndex = vIndex + 1

        }
        else {
          vFoundCount = vFoundCount - 1
          vIndex = vIndex + 1
        }

        if (vFoundCount == 0  )
        {
          println('\n\nNot Found\n')
          findings << new Infraction(
                  element: elementName,
                  message: "Element $elementName does not contain " + this.childnode,
                  lineNumber: findLineNumber(node),
                  category: 'InvalidChild')
          passing = false
          elementFound = false
          print('findings : ' + findings)
        }
        else
        {
          println('\nFinal vFoundCount : ' + vFoundCount)
          passing = true
          elementFound = true
        }

      print('\nvFoundCount : ' + vFoundCount)
    }

    vFoundCount = 0
  }

  void validateAttributeValue(Node node, NodeChecker nodeChecker, String mode) {
    if (children == null) return

    if (this.nodeValue.contains(node.children())) return

    def parent = node.parent()
    def inputNodeValue = node.attribute(this.attrib)

    if (inputNodeValue != null) {

      if (inputNodeValue != nodeValue) {
        findings << new Infraction(
                element: elementName,
                message: "Attribute Value is not a equal to expected value",
                lineNumber: findLineNumber(node),
                category: 'InvalidValue')

        passing = false
      } else {
        passing = true
      }
    }
  }

  void validateAttributeContainsValue(Node node, NodeChecker nodeChecker) {
    if (this.containsText == null) return

  //  if (this.nodeValue == null || this.attrib) return

    //if (this.nodeValue.contains(node.children())) return
    println('this.containsText + ' + this.containsText)
    println('this.nodeValue + ' + this.nodeValue)
    println('this.attrib + ' + this.attrib)
    println('nodeValue + ' + nodeValue)


    def inputNodeValue = node.attribute(this.attrib)

    if (inputNodeValue != null) {

      def contains = inputNodeValue.toString().matches(nodeValue)
      //def contains = inputNodeValue.toString().contains(nodeValue)

      if (contains != true) {
        println('333')
        findings << new Infraction(
                attribute: elementName,
                value: nodeValue,
                message: "Attribute $elementName does not contain expected value " + nodeValue,
                lineNumber: findLineNumber(node),
                category: 'InvalidValue')

        passing = false
      } else {

        passing = true
      }

    }
  }

  void validateAttributeContainsText(Node node, NodeChecker nodeChecker) {

    if (this.script == null) return


    println('vFoundCount : ' + vFoundCount)
    println('Inside validateAttributeContainsText : ')
    println('Inside validateAttributeContainsText : \n+ Node + : ' + node + ' \n+ this.nodevalue : ' + this.nodeValue )
    println('Inside validateAttributeContainsText : \n+ Node.children + : ' + node.children()  )

    //def parent = node.parent()
    print('attrib : ' + this.attrib + ' | node ' + node.children())

    def inputNodeValue = node.children()

    if (inputNodeValue != null) {

      println('\ninside if - script ' + script )

      def contains = script.toString().contains(inputNodeValue)
      //def contains = inputNodeValue.toString().contains(nodeValue)

      println('\ninputNodeValue ' + inputNodeValue
              + '\nscript ' + script +
              '\ncontains ' + contains )

      if (contains != true) {
        println('333')
        findings << new Infraction(
                attribute: elementName,
                value: nodeValue,
                message: "Attribute $elementName does not contain expected value " + nodeValue,
                lineNumber: findLineNumber(node),
                category: 'InvalidValue')

        passing = false
      }
      else
      {
        passing = true
      }
    }
  }

  void validateParent(Node node, NodeChecker nodeChecker) {
    if (parent != null) {
      passing = false

      if (!nodeChecker.isMatch(node.parent(), parent)) {
        findings << new Infraction(
                element: elementName,
                message: "Element $elementName does not have a parent of $parent",
                lineNumber: findLineNumber(node),
                category: 'InvalidParent')
      }
      else {
        passing = true
      }
    }
  }
}