package org.deloitte.dpe.aggregator

import org.deloitte.dpe.NodeChecker

interface Aggregator {
  void reset()

  void handleNode(String filename, Node node, NodeChecker nodeChecker)

  Map<String, Integer> getTotals()
}