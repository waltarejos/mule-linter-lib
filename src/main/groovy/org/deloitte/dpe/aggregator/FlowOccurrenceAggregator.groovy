package org.deloitte.dpe.aggregator

import org.deloitte.dpe.NodeChecker

class FlowOccurrenceAggregator implements Aggregator {
  int flowCount

  FlowOccurrenceAggregator() {
    init()
  }

  void init() {
    flowCount = 0
  }

  void reset() {
    init()
  }

  void handleNode(String filename, Node node, NodeChecker nodeChecker) {
    if (nodeChecker.isMatch(node, 'flow')) {
      ++flowCount
    }
    if (nodeChecker.isMatch(node, 'sub-flow')) {
      ++flowCount
    }
  }

  Map<String, Integer> getTotals() {
    return ['flowCount': flowCount]
  }
}
